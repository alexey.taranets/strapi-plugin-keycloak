const jwt = require("jsonwebtoken");
const JwksClient = require("jwks-rsa");

function getJwtCertificateFromPublicKey(publicKey) {
  return `-----BEGIN CERTIFICATE-----
${publicKey}
-----END CERTIFICATE-----
`;
}

function getJwtPublicKeyFromPublicKey(publicKey) {
  return `-----BEGIN PUBLIC KEY-----
${publicKey}
-----END PUBLIC KEY-----
`;
}

function getSecretOrPublicKeyBasedOnAlgorithm(publicKey, algorithm) {
  return algorithm === "RS256"
    ? getJwtPublicKeyFromPublicKey(publicKey)
    : getJwtCertificateFromPublicKey(publicKey);
}

async function getJwtValidityAndPayload(
  token,
  { algorithm, publicKey, jwksUri }
) {
  try {
    let payload;

    if (jwksUri) {
      const jwksClient = JwksClient({
        jwksUri,
      });

      const getKey = (header, callback) => {
        jwksClient.getSigningKey(header.kid, function (err, key) {
          var signingKey = key.publicKey || key.rsaPublicKey;
          callback(null, signingKey);
        });
      };

      try {
        payload = await new Promise((resolve, reject) =>
          // `jwt.verify` returns its callback immediately, but the library still forces us
          // to use a callback. Hence, we wrap it into `setTimeout` to allow the promise use.
          // eslint-disable-next-line no-undef
          setTimeout(() => {
            jwt.verify(token, getKey, (err, succ) => {
              if (err) {
                return reject(err);
              }
              resolve(succ);
            });
          }, 0)
        );
        return { valid: true, payload };
      } catch (err) {
        return {
          valid: false,
        };
      }
    } else {
      payload = jwt.verify(
        token,
        getSecretOrPublicKeyBasedOnAlgorithm(publicKey, algorithm),
        {
          algorithms: [algorithm],
        }
      );
    }

    return {
      valid: true,
      payload,
    };
  } catch (err) {
    return { valid: false, payload: null };
  }
}

function getJwtPayload(token) {
  const payload = jwt.decode(token);
  return payload;
}

function getJwtExpiry(token) {
  const payload = getJwtPayload(token);
  return payload?.exp ?? 0;
}

function signJwt(payload, secret) {
  return jwt.sign(payload, secret);
}

module.exports = {
  getJwtValidityAndPayload,
  getJwtExpiry,
  getJwtPayload,
  signJwt,
  getJwtCertificateFromPublicKey,
};

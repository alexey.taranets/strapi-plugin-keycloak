const getProfile = require("./get-profile");
jest.mock("node-fetch", () => jest.fn());

describe("getProfile", () => {
  beforeEach(() => {
    global.strapi = {
      config: {
        keycloak: {},
      },
    };
  });

  it("sould return null if no access token is set", async () => {
    const result = await getProfile({});
    expect(result).toBeNull();
  });
});

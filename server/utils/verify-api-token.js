/**
 * @param {*} ctx
 * @returns resolves to `true` if a valid API token is set
 */
module.exports = async (ctx) => {
  const apiTokenService = strapi.services?.["admin::api-token"];

  let apiTokenFromHeaders = ctx.request?.header?.authorization?.trim();

  if (!apiTokenFromHeaders || !apiTokenService) {
    return;
  }

  if (apiTokenFromHeaders.toLowerCase().startsWith("bearer ")) {
    apiTokenFromHeaders = apiTokenFromHeaders.substr("bearer ".length);
  }

  const apiTokenFromStrapi = await apiTokenService.getBy({
    accessKey: apiTokenService.hash(apiTokenFromHeaders),
  });

  return !!apiTokenFromStrapi;
};

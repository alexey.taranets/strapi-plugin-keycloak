const getProfile = require("./get-profile");

/**
 * @returns an object in the shape of
 *
 * ```json
 * {
 *  isLoggedIn: boolean,
 *  profile?: keycloak user profile
 * }
 * ```
 *
 * `isLoggedIn` is true if a user is authenticated. The `profile` is filled with the Keycloak user profile then.
 */
module.exports = async (ctx) => {
  const profile = await getProfile(ctx);
  return {
    isLoggedIn: profile != null && profile.error == null,
    profile,
  };
};

function getTokenFromContext(ctx, tokenType = "accessToken") {
  return ctx.session?.keycloak?.[tokenType];
}

module.exports = { getTokenFromContext };

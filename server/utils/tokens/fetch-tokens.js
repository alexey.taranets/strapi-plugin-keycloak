const fetch = require("node-fetch");

/**
 * Calls the token endpoint with the given request body (a multipart/form-data string).
 *
 * Returns `idToken`, `accessToken` and `refreshToken` if successful, otherwise returns an `error` object.
 */
async function fetchTokenWithBody(body) {
  const { tokenEndpoint } = strapi.config.keycloak;
  const response = await fetch(`${tokenEndpoint}`, {
    method: "POST",
    body,
    headers: {
      "content-type": "application/x-www-form-urlencoded",
    },
  });

  const {
    access_token: accessToken,
    id_token: idToken,
    refresh_token: refreshToken,
    error,
  } = await response.json();

  return {
    accessToken,
    idToken,
    refreshToken,
    error,
  };
}

async function fetchAccessToken({ code }) {
  const { clientId, clientSecret, redirectUri } = strapi.config.keycloak;
  return fetchTokenWithBody(
    `grant_type=authorization_code&code=${code}&redirect_uri=${redirectUri}&client_id=${clientId}&client_secret=${clientSecret}`
  );
}

async function refreshAccessToken({ refreshToken }) {
  const { clientId, clientSecret } = strapi.config.keycloak;
  const {
    accessToken,
    idToken,
    refreshToken: newRefreshToken,
    error,
  } = await fetchTokenWithBody(
    `grant_type=refresh_token&client_id=${clientId}&client_secret=${clientSecret}&refresh_token=${refreshToken}&scope=${encodeURIComponent(
      "openid profile"
    )}`
  );

  return {
    accessToken,
    idToken,
    refreshToken: newRefreshToken,
    error,
  };
}

module.exports = {
  fetchAccessToken,
  refreshAccessToken,
};

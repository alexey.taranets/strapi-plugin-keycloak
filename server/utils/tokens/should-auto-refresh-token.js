const { getJwtExpiry } = require("../jwt");
const { getTokenFromContext } = require("./get-token-from-context");

function shouldAutoRefreshToken(ctx) {
  const { autoRefreshMsBeforeExpiry } = strapi.config.keycloak;

  if (!autoRefreshMsBeforeExpiry) {
    return false;
  }

  const accessToken = getTokenFromContext(ctx, "accessToken");
  const expiry = getJwtExpiry(accessToken);

  return expiry * 1_000 < new Date().getTime() + autoRefreshMsBeforeExpiry;
}

module.exports = { shouldAutoRefreshToken };
